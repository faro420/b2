package array;

/**
 * @author Mir Farshid Baha
 * ArrayList offers methods to manage an unchecked list of elements with the
 * same reference type
 */
public class ArrayList<T> {
    // array object variable for the list
    private T[] list;
    // current number of elements in the list
    private int size;

    public boolean isEmpty() {
        return size == 0;
    }

    public void printF2L() {
        if (!isEmpty()) {
            for (int i = 0; i < size; i++)
                System.out.println(list[i].toString());
        }
    }

    public void printL2F() {
        if (!isEmpty()) {
            for (int i = 0; i < size; i++)
                System.out.println(list[(size - 1) - i].toString());
        }
    }

    public T peekF() {
        T test = null;
        if (!isEmpty())
            test = list[0];
        return test;
    }

    public T peekL() {
        T test = null;
        if (!isEmpty())
            test = list[size - 1];
        return test;
    }

    public T peekNo(int n) {
        T test = null;
        if (!isEmpty() && 0 <= n && n < size)
            test = list[n];
        return test;
    }

    public T getF() {
        T puffer = null;
        if (!isEmpty()) {
            puffer = list[0];
            for (int i = 0; i < size - 1; i++)
                list[i] = list[i + 1];
            list[size - 1] = null;
            size--;
        }
        return puffer;
    }

    public T getL() {
        T puffer = null;
        if (!isEmpty()) {
            puffer = list[size - 1];
            list[size - 1] = null;
            size--;
        }
        return puffer;
    }

    public T getNo(int n) {
        T puffer = null;
        if (!isEmpty() && 0 <= n && n < size) {
            puffer = list[n];
            for (int i = n; i < size; i++) {
                list[i] = list[i + 1];
            }
            list[size - 1] = null;
            size--;
        }
        return puffer;
    }

    public int pos(T t) {
        int position = -1;
        if (!isEmpty() && t != null) {
            for (int i = 0; i < size; i++) {
                if (t.equals(list[i])) {
                    position = i;
                    i = size;
                }
            }
        }
        return position;
    }

    @SuppressWarnings("unchecked")
    public void putF(T t) {
        if (t != null) {
            if (isEmpty()) {
                list = (T[]) new Object[1];
                list[0] = t;
            } else {
                if (list.length == size) {
                    T[] newList = (T[]) new Object[2 * list.length];
                    for (int i = 0; i < size; i++) {
                        newList[i + 1] = list[i];
                    }
                    newList[0] = t;
                    list = newList;
                } else {

                    for (int i = 0; i < size; i++) {
                        list[size - i] = list[size - i - 1];
                    }
                    list[0] = t;
                }
            }
            size++;
        }
    }

    @SuppressWarnings("unchecked")
    public void putL(T t) {
        if (t != null) {
            if (isEmpty()) {
                list = (T[]) new Object[1];
                list[0] = t;
            } else {
                if (list.length == size) {
                    T[] newList = (T[]) new Object[2 * list.length];
                    for (int i = 0; i < size; i++) {
                        newList[i] = list[i];
                    }
                    newList[size] = t;
                    list = newList;

                } else {
                    list[size] = t;

                }
            }
            size++;
        }
    }

    @SuppressWarnings("unchecked")
    public boolean putNo(int n, T t) {
        boolean test = false;
        if (t != null && 0 <= n && n <= size) {
            if (isEmpty()) {
                list = (T[]) new Object[1];
                list[0] = t;
                test = true;
            } else {
                if (list.length == size) {
                    list = (T[]) new Object[2 * list.length];
                }
                for (int i = 0; i < n + 1; i++) {
                    list[size - i] = list[size - i - 1];
                }
                list[n] = t;
                test = true;
            }
            size++;
        }
        return test;
    }

    public T setNo(int n, T t) {
        T puffer = null;
        if (peekNo(n) != null && t != null) {
            puffer = peekNo(n);
            list[n] = t;
        }
        return puffer;
    }

    public int size() {
        return size;
    }

    public boolean remove(T t) {
        boolean test = false;
        int n = pos(t);
        if (n != -1) {
            for (int i = n; i < size; i++) {
                list[i] = list[i + 1];
            }
            list[size] = null;
            size--;
            test = true;
        }
        return test;
    }

    public ArrayList<T> sublist(T t) {
        ArrayList<T> object = new ArrayList<T>();
        int n = pos(t);
        if (n != -1 && 0 < size - n) {
            @SuppressWarnings("unchecked")
            T[] newList = (T[]) new Object[size - n + 1];
            for (int i = 0; i < newList.length; i++) {
                newList[i] = list[n + i];
            }
            for (int j = 0; j < newList.length; j++) {
                object.putL(newList[j]);
            }
        } else {
            object = null;
        }
        return object;
    }

    public ArrayList<T> sublist(T t1, T t2) {
        ArrayList<T> object = new ArrayList<T>();
        int n = pos(t1);
        int m = pos(t2);
        if (n != -1 && m != -1 && n != m) {
            int start, end;
            if (n > m) {
                start = m;
                end = n;
            } else {
                start = n;
                end = m;
            }
            @SuppressWarnings("unchecked")
            T[] newList = (T[]) new Object[end - start + 1];
            for (int i = 0; i < newList.length; i++) {
                newList[i] = list[start + i];
            }
            for (int j = 0; j < newList.length; j++) {
                object.putL(newList[j]);
            }
        } else {
            object = null;
        }
        return object;
    }
}
