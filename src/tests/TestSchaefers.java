package tests;
/**
 * TeamNr:S1T5 <Mir Farshid, Baha> (<2141801>,<mirfarshid.baha@haw-hamburg.de>),
 * <Mehmet, Cakir> (<2195657>,<mehmet.cakir@haw-hamburg.de>),
 */
import b2.CD;
import b2.DVD;
import b2.Disc;
import b2.NodeList;

public class TestSchaefers {
    public static void main(String[] args) {
        {
            System.out.println("-----------------------------");
            NodeList<Disc> l1 = new NodeList<Disc>(); // viel in l1 reinstecken
                                                      // (min. 16 Elemente)
            Disc[] collection = {
                    new DVD("Auf nach Sueden", DVD.Typ.VIDEO, DVD.Format.PAL),
                    new CD("Bambam", CD.Type.AUDIO, "Carlo"),
                    new CD("Huhu", CD.Type.AUDIO, "Carbone"),
                    new CD("Gaspedal", CD.Type.AUDIO, "Schumi"),
                    new CD("Kreuz", CD.Type.AUDIO, "Metalico"),
                    new CD("Kreuzritter", CD.Type.AUDIO, "Metalico"),
                    new DVD("Pokemon", DVD.Typ.VIDEO, DVD.Format.NTSC),
                    new DVD("Grandizer", DVD.Typ.MOVIE, DVD.Format.PAL),
                    new CD("Unknown", CD.Type.AUDIO, "Stevie Wonder"),
                    new DVD("Transformers", DVD.Typ.VIDEO, DVD.Format.PAL),
                    new DVD("Augsburger Puppenkiste: Urmel aus dem Eis � Gold Edition", DVD.Typ.MOVIE, DVD.Format.PAL),
                    new DVD("Sin City - Uncut", DVD.Typ.MOVIE, DVD.Format.NTSC),
                    new DVD("Gone with the Wind", DVD.Typ.MOVIE, DVD.Format.PAL),
                    new DVD("David Bowie: Best of Bowie", DVD.Typ.VIDEO,
                            DVD.Format.PAL),
                    new CD("IV", CD.Type.AUDIO, "Led Zeppelin"),
                    new CD("Sing mit Heino", CD.Type.AUDIO, "Heino") };
            for (int i = 0; i < 16; i++)
                l1.putL(collection[i]);
    
            NodeList<Disc> l2 = new NodeList<Disc>(); // l2 ist am Anfang leer
            // nun testen durch �hin-und-her�-kopieren
            if (l1.size() < 16) {
                System.out
                        .println("ERROR : Die Aufgabenstellung wurde nicht gruendlich gelesen!");
            }
            while (!l1.isEmpty()) {
                l2.putF(l1.getL());
            }
            l2.printF2L();
            System.out.println(l2.size());
            
            while (!l2.isEmpty()) {
                l1.putL(l2.getF());
            }
    
            l1.printF2L();
            System.out.println(l1.size());
            
            while (!l1.isEmpty()) {
                l2.putNo(0, l1.getL());
            }
            l2.printF2L();
            System.out.println(l2.size());
            
            while (!l2.isEmpty()) {
                l1.putNo(l1.size(), l2.getF());
            }
            l1.printF2L();
            System.out.println(l1.size());
            while (!l1.isEmpty()) {
                l2.putNo((int) (Math.random() * (1 + l2.size())), l1.getL());
            }
            l2.printF2L();
            
            System.out.println(l2.size());
            while (!l2.isEmpty()) {
                Disc d = l2.peekNo((int) (Math.random() * l2.size()));
                l2.remove(d);
                l1.putNo((int) (Math.random() * (1 + l1.size())), d);
            }// while
            l1.printF2L();
            System.out.println(l1.size());
            l1.printL2F();
            System.out.println(l1.size());
            l1.printF2L();
            System.out.println(l1.size());
            l2.printF2L();
            System.out.println(l2.size());
            l2.printL2F();
            System.out.println(l2.size());
            System.out.println("-----------------------------");
            
            // subList testen
            // System.out.println("test");
            // l1.printF2L();
            // System.out.println("test Ende");
            System.out.println(l1.peekF().toString()); 
            l1.subList(l1.peekNo(4), l1.peekNo(13)).printL2F();          System.out.printf("%d->%d:%d\n", 4, 13,l1.subList(l1.peekNo(4), l1.peekNo(13)).size());
            l1.subList(l1.peekNo(13), l1.peekNo(4)).printL2F();          System.out.printf("%d->%d:%d\n", 13, 4,l1.subList(l1.peekNo(13), l1.peekNo(4)).size());
            System.out.println("-----------------------------");
            
            int i1 = (int) (Math.random() * (l1.size()));
            int i2 = (int) (Math.random() * (l1.size()));
            int e1 = l1.size() - 1;
            System.out.printf("\n i1=%d , i2=%d , size=%d\n", i1, i2, l1.size());                     l1.subList(l1.peekNo(i1), l1.peekNo(i2)).printL2F();
            System.out.printf("%d->%d:%d\n", i1, i2,l1.subList(l1.peekNo(i1), l1.peekNo(i2)).size()); l1.subList(l1.peekF(), l1.peekNo(i1)).printL2F();
            System.out.printf("%d->%d:%d\n", 0, i1,l1.subList(l1.peekF(), l1.peekNo(i1)).size());     l1.subList(l1.peekL(), l1.peekNo(i1)).printL2F();
            System.out.printf("%d->%d:%d\n", e1, i1,l1.subList(l1.peekL(), l1.peekNo(i1)).size());    l1.subList(l1.peekNo(i1), l1.peekF()).printL2F();
            System.out.printf("%d->%d:%d\n", i1, 0,l1.subList(l1.peekNo(i1), l1.peekF()).size());     l1.subList(l1.peekNo(i1), l1.peekL()).printL2F();
            System.out.printf("%d->%d:%d\n", i1, e1,l1.subList(l1.peekNo(i1), l1.peekL()).size());
            System.out.println("-----------------------------");
        }
        System.out.println("###########################################################################################################");
        System.out.println("###########################################################################################################");
        System.out.println("###########################################################################################################");
        System.out.println("###########################################################################################################");
        System.out.println("###########################################################################################################");
        System.out.println("\n\n\n\n");
        System.out.println("###########################################################################################################");
        System.out.println("###########################################################################################################");
        System.out.println("###########################################################################################################");
        System.out.println("###########################################################################################################");
        System.out.println("###########################################################################################################");
        
        
        
        
        {
            System.out.println( "-----------------------------" );
            NodeList<Disc> l1 = new NodeList<Disc>(); // viel in l1 reinstecken - (min. 16 Elemente)
            Disc[] collection = {
                new DVD("Auf nach Sueden", DVD.Typ.VIDEO, DVD.Format.PAL),
                new CD("Bambam", CD.Type.AUDIO, "Carlo"),
                new CD("Huhu", CD.Type.AUDIO, "Carbone"),
                new CD("Gaspedal", CD.Type.AUDIO, "Schumi"),
                new CD("Kreuz", CD.Type.AUDIO, "Metalico"),
                new CD("Kreuzritter", CD.Type.AUDIO, "Metalico"),
                new DVD("Pokemon", DVD.Typ.VIDEO, DVD.Format.NTSC),
                new DVD("Grandizer", DVD.Typ.MOVIE, DVD.Format.PAL),
                new CD("Unknown", CD.Type.AUDIO, "Stevie Wonder"),
                new DVD("Transformers", DVD.Typ.VIDEO, DVD.Format.PAL),
                new DVD("Augsburger Puppenkiste: Urmel aus dem Eis � Gold Edition", DVD.Typ.MOVIE, DVD.Format.PAL),
                new DVD("Sin City - Uncut", DVD.Typ.MOVIE, DVD.Format.NTSC),
                new DVD("Gone with the Wind", DVD.Typ.MOVIE, DVD.Format.PAL),
                new DVD("David Bowie: Best of Bowie", DVD.Typ.VIDEO, DVD.Format.PAL),
                new CD("IV", CD.Type.AUDIO, "Led Zeppelin"),
                new CD("Sing mit Heino", CD.Type.AUDIO, "Heino")
            };
            for (int i = 0; i < 16; i++){
                 l1.putL(collection[i]);
            }

            NodeList<Disc> l2 = new NodeList<Disc>(); // l2 ist am Anfang leer

            //nun testen durch �hin-und-her�-kopieren
            if ( l1.size() < 16 ){ System.out.println( "ERROR : Die Aufgabenstellung wurde nicht gruendlich gelesen!" ); }
            while ( !l1.isEmpty() ){ l2.putF( l1.getL() ); } l2.printF2L(); System.out.println( l2.size() );
            while ( !l2.isEmpty() ){ l1.putL( l2.getF() ); } l1.printF2L(); System.out.println( l1.size() );
            while ( !l1.isEmpty() ){ l2.putNo( 0, l1.getL() ); } l2.printF2L(); System.out.println( l2.size() );
            while ( !l2.isEmpty() ){ l1.putNo( l1.size(), l2.getF() ); } l1.printF2L(); System.out.println( l1.size() );
            while ( !l1.isEmpty() ){ l2.putNo( (int)( Math.random()*(1+l2.size()) ), l1.getL() ); } l2.printF2L(); System.out.println( l2.size() );
            while ( !l2.isEmpty() ){
                Disc d = l2.peekNo( (int)( Math.random()*l2.size() ) );
                l2.remove( d );
                l1.putNo( (int)( Math.random()*(1+l1.size()) ), d );
            }//while
            l1.printF2L(); System.out.println( l1.size() );
            l1.printL2F(); System.out.println( l1.size() );
            l1.printF2L(); System.out.println( l1.size() );
            l2.printF2L(); System.out.println( l2.size() );
            l2.printL2F(); System.out.println( l2.size() );
            System.out.println( "-----------------------------" );
            //subList testen
            l1.subList(l1.peekNo(4), l1.peekNo(13)).printL2F(); System.out.printf("%d->%d:%d\n",  4,13, l1.subList( l1.peekNo(4), l1.peekNo(13)).size());
            l1.subList(l1.peekNo(13),l1.peekNo(4) ).printL2F(); System.out.printf("%d->%d:%d\n", 13, 4, l1.subList( l1.peekNo(13),l1.peekNo(4) ).size());
            System.out.println( "-----------------------------" );
            int i1 = (int)( Math.random()*(l1.size()) );
            int i2 = (int)( Math.random()*(l1.size()) );
            int e1 = l1.size() - 1;
            System.out.printf( "\n i1=%d , i2=%d , size=%d\n", i1, i2, l1.size() );
            l1.subList( l1.peekNo( i1 ), l1.peekNo( i2 ) ).printL2F(); System.out.printf("%d->%d:%d\n", i1,i2, l1.subList( l1.peekNo(i1),l1.peekNo(i2) ).size());
            l1.subList( l1.peekF(), l1.peekNo( i1 ) ).printL2F(); System.out.printf("%d->%d:%d\n", 0,i1, l1.subList( l1.peekF(), l1.peekNo( i1 ) ).size());
            l1.subList( l1.peekL(), l1.peekNo( i1 ) ).printL2F(); System.out.printf("%d->%d:%d\n", e1,i1, l1.subList( l1.peekL(), l1.peekNo( i1 ) ).size());
            l1.subList( l1.peekNo( i1 ), l1.peekF() ).printL2F(); System.out.printf("%d->%d:%d\n", i1, 0, l1.subList( l1.peekNo( i1 ),l1.peekF() ).size());
            l1.subList( l1.peekNo( i1 ), l1.peekL() ).printL2F(); System.out.printf("%d->%d:%d\n", i1,e1, l1.subList( l1.peekNo( i1 ),l1.peekL() ).size());
            System.out.println( "-----------------------------" );
        }   
    }
}
