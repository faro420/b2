package tests;
/**
 * TeamNr:S1T5 <Mir Farshid, Baha> (<2141801>,<mirfarshid.baha@haw-hamburg.de>),
 * <Mehmet, Cakir> (<2195657>,<mehmet.cakir@haw-hamburg.de>),
 */
import b2.CD;
import b2.DVD;
import b2.Disc;
import b2.NodeList;

public class NodeListTest {
    public static void main(String[] args) {
        // list.putL(cd1);
        // list.putL(cd2);
        // list.putL(cd3);
        // System.out.println(cd1.toString());
        // list.putL(cd4);
        // list.putL(cd5);

        // ------------------------isEmpty------------------------------
        /*
         * Tested with and without elements in Listwith elements falsewithout
         * elements true
         */
        // list.putF(cd1);
        // list.putL(cd2);
        // list.putL(cd3);
        // list.putF(cd4);
        // list.putF(cd5);
        // System.out.print(list.isEmpty());

        // ------------------------printF2L-----------------------------

        // list.putF(cd1);
        // list.putL(cd2);
        // list.putL(cd3);
        // list.putF(cd4);
        // list.putL(cd5);
        // list.printF2L();
        // expected order in List: cd4, cd1, cd2, cd3, cd5
        // expected printF2L output: cd4, cd1, cd2, cd3, cd5

        // ------------------------printL2F-----------------------------

        // list.putL(cd1);
        // list.putL(cd2);
        // list.putF(cd3);
        // list.putL(cd4);
        // list.putF(cd5);
        // list.printL2F();
        // expected order in List: cd5, cd3, cd1, cd2, cd4
        // expected printL2F output: cd4, cd2, cd1, cd3, cd5

        // ------------------------peekF--------------------------------

        // list.putF(cd1);
        // list.putL(cd2);
        // list.putL(cd3);
        // list.putF(cd4);
        // list.putF(cd5);
        // System.out.print(list.peekF());
        // expected order in List: cd5, cd4, cd1, cd2, cd3
        // expected peekF: cd5

        // ------------------------peekL--------------------------------

        // list.putF(cd1);
        // list.putL(cd2);
        // list.putF(cd3);
        // list.putL(cd4);
        // list.putF(cd5);
        // System.out.print(list.peekL());
        // expected order in List: cd5, cd3, cd1, cd2, cd4
        // expected peekL: cd4

        // ------------------------peekNo-------------------------------

        // list.putL(cd1);
        // list.putF(cd2);
        // list.putL(cd3);
        // list.putF(cd4);
        // list.putL(cd5);
        // System.out.print(list.peekNo(3));
        // expected order in List: cd4, cd2, cd1, cd3, cd5
        // expected peekNo: dynamic - because value corresponds cd-position
        // list.printF2L();
        // expected pF2L output: the order like the cds are in the list

        // ------------------------getF---------------------------------

        // list.putF(cd1);
        // list.putF(cd2);
        // list.putL(cd3);
        // list.putL(cd4);
        // list.putF(cd5);
        // list.getF();
        // expected order in list: cd5, cd2, cd1, cd3, cd4
        // list.printF2L();
        // expected printF2L output: cd2, cd1, cd3, cd4
        // ------------------------getL---------------------------------

        // list.putL(cd1);
        // list.putL(cd2);
        // list.putF(cd3);
        // list.putF(cd4);
        // list.putL(cd5);
        // list.getL();
        // expected order in list: cd4, cd3, cd1, cd2, cd5
        // list.printF2L();
        // expected printF2L output: cd4, cd3, cd1, cd2

        // ------------------------getNo--------------------------------

        // list.putL(cd1);
        // list.putF(cd2);
        // list.putF(cd3);
        // list.putF(cd4);
        // list.putL(cd5);
        // list.getNo(2);
        // expected order in list: cd4, cd3, cd2, cd1, cd5
        // list.printF2L();
        // expected printF2L output: dynamic, cause the value in getNo method
        // removes corresponding position content
        // ------------------------pos----------------------------------

        // list.putF(cd1);
        // list.putL(cd2);
        // list.putL(cd3);
        // list.putF(cd4);
        // list.putF(cd5);
        // expected order in list: cd5, cd4, cd1, cd2, cd3
        // System.out.println("Position:" + list.pos(cd3));
        // expected output: information of cd which is typed in pos method else
        // -1

        // ------------------------putF---------------------------------

        // list.putF(cd1);
        // list.putL(cd2);
        // list.putL(cd3);
        // list.putL(cd4);
        // list.putF(cd5);
        // expected order in list: cd5, cd1, cd2, cd3, cd4
        // list.printF2L();
        // expected pF2L: like order in list
        // ------------------------putL---------------------------------

        // list.putF(cd1);
        // list.putL(cd2);
        // list.putL(cd3);
        // list.putF(cd4);
        // list.putL(cd5);
        // expected order in list: cd4, cd1, cd2, cd3, cd5
        // list.printF2L();
        // expected pF2L: like order in list
        // ------------------------putNo--------------------------------

        // list.putF(cd1);
        // list.putL(cd2);
        // list.putL(cd3);
        // list.putF(cd4);
        // list.putF(cd5);
        // expected order in list: cd5, cd4, cd1, cd2, cd3
        // System.out.println(list.putNo(2, cd6));
        // expected pF2L: cd5, cd4, cd1, cd6, cd2, cd3
        // list.printF2L();
        // ------------------------setNo--------------------------------

        // list.putL(cd1);
        // list.putF(cd2);
        // list.putF(cd3);
        // list.putL(cd4);
        // list.putL(cd5);
        // expected order in list: cd3, cd2, cd1, cd4, cd5
        // System.out.println(list.setNo(3, cd6));
        // list.printF2L();
        // expected pF2L: dynamic
        // ------------------------size---------------------------------

        // list.putF(cd1);
        // list.putL(cd2);
        // list.putL(cd3);
        // list.putF(cd4);
        // list.putF(cd5);
        // list.size();

        // ------------------------remove-------------------------------

        // list.putL(cd1);
        // list.putF(cd2);
        // list.putL(cd3);
        // list.putF(cd4);
        // list.putF(cd5);
        // expected order in list: cd5, cd4, cd2, cd1, cd3
        // list.printF2L();
        // System.out.print(list.remove(cd5));
        // System.out.print(list.remove(cd2));
        // System.out.print(list.remove(cd3));
        // list.printF2L();

        // ------------------------subList------------------------------
        // list.putL(cd1);
        // list.putL(cd2);
        // list.putL(cd3);
        // list.putL(cd4);
        // list.putL(cd5);
        // list.putL(cd6);
        // list.subList(cd4, cd6).printF2L();

        // ------------------------other--------------------------------
        // list.putF(cd1);
        // list.putL(cd2);
        // list.putL(cd3);
        // list.putF(cd4);
        // list.putF(cd5);
        // list.printL2F();

        // list.sublist(cd5, cd1).printL2F();
        // System.out.println(list.pos(cd1));
        // System.out.println(list.pos(cd2));
        // System.out.println(list.pos(cd3));
        // System.out.println(list.pos(cd4));
        // System.out.println(list.pos(cd5));
        // // System.out.println(list.pos(cd6));
        // System.out.println(list.size());
        System.out.println("-----------------------------");
        NodeList<Disc> l1 = new NodeList<Disc>(); // viel in l1 reinstecken
                                                  // (min. 16 Elemente)
        Disc[] collection = {
                new DVD("Auf nach Sueden", DVD.Typ.VIDEO, DVD.Format.PAL),
                new CD("Bambam", CD.Type.AUDIO, "Carlo"),
                new CD("Huhu", CD.Type.AUDIO, "Carbone"),
                new CD("Gaspedal", CD.Type.AUDIO, "Schumi"),
                new CD("Kreuz", CD.Type.AUDIO, "Metalico"),
                new CD("Kreuzritter", CD.Type.AUDIO, "Metalico"),
                new DVD("Pokemon", DVD.Typ.VIDEO, DVD.Format.NTSC),
                new DVD("Grandizer", DVD.Typ.MOVIE, DVD.Format.PAL),
                new CD("Unknown", CD.Type.AUDIO, "Stevie Wonder"),
                new DVD("Transformers", DVD.Typ.VIDEO, DVD.Format.PAL),
                new DVD(
                        "Augsburger Puppenkiste: Urmel aus dem Eis � Gold Edition",
                        DVD.Typ.MOVIE, DVD.Format.PAL),
                new DVD("Sin City - Uncut", DVD.Typ.MOVIE, DVD.Format.NTSC),
                new DVD("Gone with the Wind", DVD.Typ.MOVIE, DVD.Format.PAL),
                new DVD("David Bowie: Best of Bowie", DVD.Typ.VIDEO,
                        DVD.Format.PAL),
                new CD("IV", CD.Type.AUDIO, "Led Zeppelin"),
                new CD("Sing mit Heino", CD.Type.AUDIO, "Heino") };
        for (int i = 0; i < 16; i++)
            l1.putL(collection[i]);
        NodeList<Disc> l2 = new NodeList<Disc>(); 
        l1.subList(l1.peekNo(13), l1.peekNo(15)).printF2L();
        while (!l1.isEmpty()) {
            l2.putF(l1.getL());
        }
        System.out.println(l1.size());
   }
}
