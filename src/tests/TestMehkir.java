package tests;

import b2.CD;
import b2.DVD;
import b2.Disc;
import b2.NodeList;

public class TestMehkir {
    public static void main(String[] args){
    
    NodeList<Disc> list = new NodeList<Disc>();
    
        Disc cd1 = new DVD("Auf nach Sueden1", DVD.Typ.VIDEO, DVD.Format.PAL);
        Disc cd2 = new CD("Bambam2", CD.Type.AUDIO, "Carlo");
        Disc cd3 = new CD("Huhu3", CD.Type.AUDIO, "Carbone");
        Disc cd4 = new CD("Gaspedal4", CD.Type.AUDIO, "Schumi");
        Disc cd5 = new CD("Kreuz5", CD.Type.AUDIO, "Metalico");
        Disc cd6 = new CD("Kreuzritter6", CD.Type.AUDIO, "Metalico");
        Disc cd7 = new DVD("Pokemon7", DVD.Typ.VIDEO, DVD.Format.NTSC);
        Disc cd8 = new DVD("Grandizer8", DVD.Typ.MOVIE, DVD.Format.PAL);

//        l1.putL(cd1);
//        l1.putL(cd2);
//        l1.putL(cd3);
//        l1.putL(cd4);
//        l1.putL(cd5);
//        l1.putL(cd6);
//        l1.putL(cd7);
        // l1.putL(cd8);

        // System.out.println(l1.subList(cd8));

        
        // list.putL(cd1);
        // list.putL(cd2);
        // list.putL(cd3);
        // System.out.println(cd1.toString());
        // list.putL(cd4);
        // list.putL(cd5);

        // ------------------------isEmpty------------------------------
        /*
         * Tested with and without elements in Listwith elements falsewithout
         * elements true
         */
        // list.putF(cd1);
        // list.putL(cd2);
        // list.putL(cd3);
        // list.putF(cd4);
        // list.putF(cd5);
        // System.out.print(list.isEmpty());

        // ------------------------printF2L-----------------------------

        // list.putF(cd1);
        // list.putL(cd2);
        // list.putL(cd3);
        // list.putF(cd4);
        // list.putL(cd5);
        // list.printF2L();
        // expected order in List: cd4, cd1, cd2, cd3, cd5
        // expected printF2L output: cd4, cd1, cd2, cd3, cd5

        // ------------------------printL2F-----------------------------

        // list.putL(cd1);
        // list.putL(cd2);
        // list.putF(cd3);
        // list.putL(cd4);
        // list.putF(cd5);
        // list.printL2F();
        // expected order in List: cd5, cd3, cd1, cd2, cd4
        // expected printL2F output: cd4, cd2, cd1, cd3, cd5

        // ------------------------peekF--------------------------------

        // list.putF(cd1);
        // list.putL(cd2);
        // list.putL(cd3);
        // list.putF(cd4);
        // list.putF(cd5);
        // System.out.print(list.peekF());
        // expected order in List: cd5, cd4, cd1, cd2, cd3
        // expected peekF: cd5

        // ------------------------peekL--------------------------------

        // list.putF(cd1);
        // list.putL(cd2);
        // list.putF(cd3);
        // list.putL(cd4);
        // list.putF(cd5);
        // System.out.print(list.peekL());
        // expected order in List: cd5, cd3, cd1, cd2, cd4
        // expected peekL: cd4

        // ------------------------peekNo-------------------------------

        // list.putL(cd1);
        // list.putF(cd2);
        // list.putL(cd3);
        // list.putF(cd4);
        // list.putL(cd5);
        // System.out.print(list.peekNo(3));
        // expected order in List: cd4, cd2, cd1, cd3, cd5
        // expected peekNo: dynamic - because value corresponds cd-position
        // list.printF2L();
        // expected pF2L output: the order like the cds are in the list

        // ------------------------getF---------------------------------

         list.putL(cd1);
         list.putF(cd2);
         list.putF(cd3);
         list.putF(cd4);
         list.putL(cd5);
         list.getL();
         list.getF();
         list.getNo(1);
//         expected order in list: cd4, cd3, cd2, cd1,  cd5
         list.printF2L();
//         expected printF2L output: cd2, cd1, cd3, cd4
        // ------------------------getL---------------------------------

        // list.putL(cd1);
        // list.putL(cd2);
        // list.putF(cd3);
        // list.putF(cd4);
        // list.putL(cd5);
        // list.getL();
        // expected order in list: cd4, cd3, cd1, cd2, cd5
        // list.printF2L();
        // expected printF2L output: cd4, cd3, cd1, cd2

        // ------------------------getNo--------------------------------

        // list.putL(cd1);
        // list.putF(cd2);
        // list.putF(cd3);
        // list.putF(cd4);
        // list.putL(cd5);
        // list.getNo(2);
        // expected order in list: cd4, cd3, cd2, cd1, cd5
        // list.printF2L();
        // expected printF2L output: dynamic, cause the value in getNo method
        // removes corresponding position content
        // ------------------------pos----------------------------------

        // list.putF(cd1);
        // list.putL(cd2);
        // list.putL(cd3);
        // list.putF(cd4);
        // list.putF(cd5);
        // expected order in list: cd5, cd4, cd1, cd2, cd3
        // System.out.println("Position:" + list.pos(cd3));
        // expected output: information of cd which is typed in pos method else
        // -1

        // ------------------------putF---------------------------------

//         list.putF(cd1);
//         list.putL(cd2);
//         list.putL(cd3);
//         list.putL(cd4);
//         list.putF(cd5);
//      //  expected order in list: cd5, cd1, cd2, cd3, cd4
//         list.printF2L();
//         //expected pF2L: like order in list
        // ------------------------putL---------------------------------

        // list.putF(cd1);
        // list.putL(cd2);
        // list.putL(cd3);
        // list.putF(cd4);
        // list.putL(cd5);
        // expected order in list: cd4, cd1, cd2, cd3, cd5
        // list.printF2L();
        // expected pF2L: like order in list
        // ------------------------putNo--------------------------------

        // list.putF(cd1);
        // list.putL(cd2);
        // list.putL(cd3);
        // list.putF(cd4);
        // list.putF(cd5);
        // expected order in list: cd5, cd4, cd1, cd2, cd3
        // System.out.println(list.putNo(2, cd6));
        // expected pF2L: cd5, cd4, cd1, cd6, cd2, cd3
        // list.printF2L();
        // ------------------------setNo--------------------------------

        // list.putL(cd1);
        // list.putF(cd2);
        // list.putF(cd3);
        // list.putL(cd4);
        // list.putL(cd5);
        // expected order in list: cd3, cd2, cd1, cd4, cd5
        // System.out.println(list.setNo(3, cd6));
        // list.printF2L();
        // expected pF2L: dynamic
        // ------------------------size---------------------------------

        // list.putF(cd1);
        // list.putL(cd2);
        // list.putL(cd3);
        // list.putF(cd4);
        // list.putF(cd5);
        // list.size();

        // ------------------------remove-------------------------------

        // list.putL(cd1);
        // list.putF(cd2);
        // list.putL(cd3);
        // list.putF(cd4);
        // list.putF(cd5);
        // expected order in list: cd5, cd4, cd2, cd1, cd3
        // list.printF2L();
        // System.out.print(list.remove(cd5));
        // System.out.print(list.remove(cd2));
        // System.out.print(list.remove(cd3));
        // list.printF2L();

        // ------------------------subList------------------------------
        // list.putL(cd1);
        // list.putL(cd2);
        // list.putL(cd3);
        // list.putL(cd4);
        // list.putL(cd5);
        // list.putL(cd6);
        // list.subList(cd4, cd6).printF2L();

        // ------------------------other--------------------------------
        // list.putF(cd1);
        // list.putL(cd2);
        // list.putL(cd3);
        // list.putF(cd4);
        // list.putF(cd5);
        // list.printL2F();

        // list.sublist(cd5, cd1).printL2F();
        // System.out.println(list.pos(cd1));
        // System.out.println(list.pos(cd2));
        // System.out.println(list.pos(cd3));
        // System.out.println(list.pos(cd4));
        // System.out.println(list.pos(cd5));
        // // System.out.println(list.pos(cd6));
        // System.out.println(list.size());
        
        
//        NodeList<Disc> l2 = new NodeList<Disc>();
//        l2.printF2L();
    }
}
