package tests;
/**
 * TeamNr:S1T5 <Mir Farshid, Baha> (<2141801>,<mirfarshid.baha@haw-hamburg.de>),
 * <Mehmet, Cakir> (<2195657>,<mehmet.cakir@haw-hamburg.de>),
 */
import b2.CD;
import b2.Disc;
import b2.CD.Type;

public class CDTest {
    public boolean testGetters() {
        Disc cd = new CD("chicken", Type.AUDIO, "little");
        return ((CD) cd).getTitle().equals("chicken")
                && ((CD) cd).getType() == Type.AUDIO
                && ((CD) cd).getInterpret().equals("little");
    }

    public boolean testEquals() {
        Disc cd1 = new CD("chicken", Type.AUDIO, "little");
        Disc cd2 = new CD("Xi", Type.AUDIO, "Xiang Xiang");
        Disc cd3 = new CD("chicken", Type.AUDIO, "little");
        Disc cd4 = new CD("Xi", Type.AUDIO, "Xiang Xiang");
        return cd1.equals(cd3) && cd2.equals(cd4);
    }

    public static void main(String[] args) {
        boolean check = true;
        CDTest test = new CDTest();
        System.out.println("class CD test");
        if (test.testGetters()) {
            System.out.println("getters:ok");
        } else {
            System.out.println("getters: fail");
            check = false;
        }
        if (test.testEquals()) {
            System.out.println("equals():ok");
        } else {
            System.out.println("equals(): fail");
            check = false;
        }
        if (check)
            System.out.println("all test passed!");
        else
            System.out.println("not working as expected");
    }
}
