package b2;
/**
 * TeamNr:S1T5 <Mir Farshid, Baha> (<2141801>,<mirfarshid.baha@haw-hamburg.de>),
 * <Mehmet, Cakir> (<2195657>,<mehmet.cakir@haw-hamburg.de>),
 */
public abstract class Disc {
    private String title;
    
    public Disc(String title){
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    @Override
    public boolean equals(Object other) {
        return other.getClass() == this.getClass() && ((Disc) other).getTitle().equals(title);
    }
}
