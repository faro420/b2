package b2;
/**
 * TeamNr:S1T5 <Mir Farshid, Baha> (<2141801>,<mirfarshid.baha@haw-hamburg.de>),
 * <Mehmet, Cakir> (<2195657>,<mehmet.cakir@haw-hamburg.de>),
 */
public class Node<T> {
    private Node<T> next;
    private T content;

    public Node<T> getNext() {
        return next;
    }

    public void setNext(Node<T> next) {
        this.next = next;
    }

    public T getContent() {
        return content;
    }

    public void setContent(T content) {
        this.content = content;
    }
}
