package b2;

/**
 * TeamNr:S1T5 <Mir Farshid, Baha> (<2141801>,<mirfarshid.baha@haw-hamburg.de>),
 * <Mehmet, Cakir> (<2195657>,<mehmet.cakir@haw-hamburg.de>),
 */
public class NodeList<T> {
    private Node<T> first;
    private int size;

    public Node<T> getFirst() {
        return first;
    }

    public boolean isEmpty() {
        return size == 0;
    }

    public void printF2L() {
        Node<T> pointer = first;
        while( pointer != null ){
            System.out.println(pointer.getContent().toString());
            pointer = pointer.getNext();
        }
    }

    public void printL2F() {
        Node<T> pointer = first;
        StringBuffer backwards = new StringBuffer();
        while( pointer != null ){
            backwards.insert( 0, "\n" );
            backwards.insert( 0, pointer.getContent().toString() );
            pointer = pointer.getNext();
        }
        System.out.println(backwards);
    }

    public T peekF() {
        return peekNo(0);
    }

    public T peekL() {
        return peekNo(size - 1);
    }

    public T peekNo(int n) {
        T container = null;
        if (!isEmpty() && 0 <= n && n < size) {
            Node<T> pointer = first;
            for (int i = 0; i < n; i++)
                pointer = pointer.getNext();
            container = pointer.getContent();
        }
        return container;
    }

    public T getF() {
        T puffer = getNo(0);
        return puffer;
    }

    public T getL() {
        T puffer = getNo(size - 1);
        return puffer;
    }

    public T getNo(int n) {
        T container = null;
        if (!isEmpty() && 0 <= n && n < size) {
            if (n == 0) {
                container = first.getContent();
                first = first.getNext();
            } else {
                Node<T> pointer = first;
                for (int i = 0; i < n - 1; i++) {
                    pointer = pointer.getNext();
                }
                container = pointer.getNext().getContent();
                pointer.setNext(pointer.getNext().getNext());
            }
            size--;
        }
        return container;
    }

    public int pos(T t) {
        int position = -1;
        if (!isEmpty() && t != null) {
            Node<T> pointer = first;
            for (int i = 0; i < size; i++) {
                if (t.equals(pointer.getContent())) {
                    position = i;
                    i = size;
                }
                pointer = pointer.getNext();
            }
        }
        return position;
    }

    public void putF(T t) {
        putNo(0, t);
    }

    public void putL(T t) {
        putNo(size, t);
    }

    public boolean putNo(int n, T t) {
        boolean test = false;
        if (t != null && 0 <= n && n <= size) {
            if (isEmpty()) {
                first = new Node<T>();
                first.setContent(t);
            } else {
                Node<T> pointer = first;
                Node<T> newContainer = new Node<T>();
                newContainer.setContent(t);
                if (n == 0) {
                    newContainer.setNext(first);
                    first = newContainer;
                } else {
                    for (int i = 0; i < n - 1; i++) {
                        pointer = pointer.getNext();
                    }
                    newContainer.setNext(pointer.getNext());
                    pointer.setNext(newContainer);
                }
            }
            test = true;
            size++;
        }
        return test;
    }

    public T setNo(int n, T t) {
        T puffer = null;
        if (t != null && 0 <= n && n <= size) {
            if (isEmpty()) {
                first = new Node<T>();
                first.setContent(t);
            } else {
                Node<T> pointer = first;
                for (int i = 0; i < n; i++) {
                    pointer = pointer.getNext();
                }
                if (pointer != null) {
                    puffer = pointer.getContent();
                    pointer.setContent(t);
                }
            }
        }
        return puffer;
    }

    public int size() {
        return size;
    }

    public boolean remove(T t) {
        boolean test = false;
        if (!isEmpty() && t != null) {
            Node<T> remove = first;
            Node<T> before = new Node<T>();
            if (remove.getContent().equals(t)) {
                before = remove.getNext();
                remove.setNext(null);
                size--;
                test = true;
                first = before;
            }
            if (!test) {
                for (int i = 0; i < size - 1; i++) {
                    before = remove;
                    remove = remove.getNext();
                    if (remove.getContent().equals(t)) {
                        before.setNext(remove.getNext());
                        remove.setNext(null);
                        size--;
                        i = size;
                        test = true;
                    }
                }
            }
        }
        return test;
    }

    public NodeList<T> subList(T t) {
        NodeList<T> newList = null;
        if (t != null) {
            boolean isInTheList = false;
            Node<T> pointer = first;
            for (int i = 0; i < size; i++) {
                if (pointer.getContent().equals(t)) {
                    isInTheList = true;
                    i = size;
                } else {
                    pointer = pointer.getNext();
                }
            }
            if (isInTheList) {
                newList = new NodeList<T>();
                do {
                    newList.putL(pointer.getContent());
                    pointer = pointer.getNext();
                } while (pointer != null);
            }
        }
        return newList;
    }

    public NodeList<T> subList(T t1, T t2) {
        NodeList<T> newList = null;
        if (t1 != null && t2 != null) {
            int m = -1, n = -1;
            Node<T> pointer1 = null, pointer2 = null, pointer = first;
            for (int i = 0; i < size && (n == -1 || m == -1); i++) {
                if (n == -1 && pointer.getContent().equals(t1)) {
                    pointer1 = pointer;
                    n = i;
                }
                if (m == -1 && pointer.getContent().equals(t2)) {
                    pointer2 = pointer;
                    m = i;
                }
                pointer = pointer.getNext();
            }
            if (n != -1 && m != -1) {
                newList = new NodeList<T>();
                int start, end;
                Node<T> head;
                if (n > m) {
                    start = m;
                    end = n;
                    head = pointer2;
                } else {
                    start = n;
                    end = m;
                    head = pointer1;
                }
                for (int j = 0; j < end - start + 1; j++) {
                    newList.putL(head.getContent());
                    head = head.getNext();
                }
            }
        }
        return newList;
    }
}
