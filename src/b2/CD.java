package b2;
/**
 * TeamNr:S1T5 <Mir Farshid, Baha> (<2141801>,<mirfarshid.baha@haw-hamburg.de>),
 * <Mehmet, Cakir> (<2195657>,<mehmet.cakir@haw-hamburg.de>),
 */
public class CD extends Disc {
    private String interpret;
    private Type t;

    public enum Type {
        AUDIO
    };

    public CD(String title, Type t, String interpret) {
        super(title);
        this.t = t;
        this.interpret = interpret;
    }

    public String getInterpret() {
        return interpret;
    }

    public Type getType() {
        return t;
    }

    @Override
    public boolean equals(Object other) {
        return super.equals(other) && ((CD) other).getType() == t && ((CD) other).getInterpret().equals(interpret);
    }

    @Override
    public String toString() {
        return String.format("[<%s>: %s, %s, %s]", CD.class.getSimpleName(), getTitle(), interpret, t);
    }
}
