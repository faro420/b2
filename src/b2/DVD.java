package b2;
/**
 * TeamNr:S1T5 <Mir Farshid, Baha> (<2141801>,<mirfarshid.baha@haw-hamburg.de>),
 * <Mehmet, Cakir> (<2195657>,<mehmet.cakir@haw-hamburg.de>),
 */
public class DVD extends Disc {
    private Typ t;
    private Format f;

    public DVD(String title, Typ t, Format f) {
        super(title);
        this.t = t;
        this.f = f;
    }

    public enum Format {
        PAL, NTSC
    };

    public enum Typ {
        VIDEO, MOVIE
    };

    public Typ getTyp() {
        return t;
    }

    public Format getFormat() {
        return f;
    }

    @Override
    public boolean equals(Object other) {
        return super.equals(other) && ((DVD) other).getFormat() == f && ((DVD) other).getTyp() == t;
    }

    @Override
    public String toString() {
        return String.format("[<%s>: %s, %s, %s]", DVD.class.getSimpleName(), getTitle(), t, f);
    }
}
