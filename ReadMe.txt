Projekt: 	B2, AufgabenzettelX, TI1-P1, SS14
Organisation: 	HAW-Hamburg, Informatik
Team: 		S1T5
Autor(en):	
Baha, Mir Farshid   (mirfarshid.baha@haw-hamburg.de)
Cakir, Mehmet	    (mehmet.cakir@haw-hamburg.de)

--------------------------------------------------


Review History
==============

140617 v1.0 failed Schafers

putNo() and getNo(), sublist(T t1,T t2) malfunctioning (index problem)

xxxL & xxxF auf xxxNo zurückführen


140617 v2.0 ok Schafers
aber equals() hat gegenwärtig unangenehme Kopplungen => erhöhter Wartungsaufwand